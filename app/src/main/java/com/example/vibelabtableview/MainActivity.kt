package com.example.vibelabtableview

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.vibelabtableview.databinding.ActivityMainBinding
import kotlin.random.Random

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val imagesList = listOf (
            R.drawable.wallpaper1,
            R.drawable.wallpaper2,
            R.drawable.wallpaper3,
            R.drawable.wallpaper4,
            R.drawable.wallpaper5,
        )

        binding.randomButton.setOnClickListener {
            when(Random.nextInt(0,4)) {
                0 -> binding.imageHolder.setImageResource(imagesList[0])
                1 -> binding.imageHolder.setImageResource(imagesList[1])
                2 -> binding.imageHolder.setImageResource(imagesList[2])
                3 -> binding.imageHolder.setImageResource(imagesList[3])
                4 -> binding.imageHolder.setImageResource(imagesList[4])
            }
        }
    }
}